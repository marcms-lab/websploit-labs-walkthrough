import requests

# Base URL of the API endpoint
BASE_URL = "http://192.168.56.110/rest/products/{}/reviews"

# Function to get reviews for a specific product ID
def get_reviews(product_id):
    response = requests.get(BASE_URL.format(product_id))
    if response.status_code == 200:
        return response.json().get('data', [])
    else:
        print(f"Failed to get reviews for product ID {product_id}")
        return []

# Dictionary to hold authors and their review counts
author_review_counts = {}

# Enumerate all reviews for product IDs from 1 to 100 and count them per author
for product_id in range(1, 101):
    reviews = get_reviews(product_id)
    for review in reviews:
        author = review['author']
        # Initialize or increment the author's review count
        author_review_counts[author] = author_review_counts.get(author, 0) + 1

# Sort authors by number of reviews in descending order
sorted_authors = sorted(author_review_counts.items(), key=lambda item: item[1], reverse=True)

# Print the sorted list of authors and their review counts
print("Authors sorted by number of reviews on descending order (number of reviews : author):")
for author, count in sorted_authors:
    print(f"{count} : {author}")