#!/bin/sh

#
# Copyright (c) 2014-2023 Marc MS & the OWASP Juice Shop contributors.
# SPDX-License-Identifier: MIT
#

# Exit on error
set -e

# Set node version
NODE_MAJOR=20

# Update
sudo apt-get update

# Add nodejs key and repository
apt-get install -y ca-certificates curl gnupg
mkdir -p /etc/apt/keyrings
curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg
echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list

# Install apache
apt-get update -q
apt-get upgrade -qy
apt-get install -qy apache2

# Install nodejs
apt-get update
apt-get install nodejs -y

# Clone juice shop repo
cd /var/www/html
git clone https://github.com/juice-shop/juice-shop.git --depth 1

# Install dependencies and start juice shop
cd juice-shop
npm install
npm start

# Put the apache config files in place
cp /tmp/juice-shop/default.conf /etc/apache2/sites-available/000-default.conf

# Enable proxy modules in apache and restart
a2enmod proxy_http
systemctl restart apache2.service