#!/bin/bash
# script to instantiate NodeGoat
# the OWASP NodeGoat repo is already cloned during installation (install.sh)

curl -sSL https://websploit.org/nodegoat-docker-compose.yml > /root/NodeGoat/docker-compose.yml
cd /root/NodeGoat
docker-compose build
docker-compose up
