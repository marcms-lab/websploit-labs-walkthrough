# WebSploit Labs Walkthrough

This is a walkthrough for WebSploit Labs.

It is a project created by security researcher Omar Santos, which has more than 450 web exploitation exercises distributed in 17 Docker containers.

This will allow us to have a testing laboratory in a safe environment and at our disposal whenever we want to use it. 

Its installation is so simple that it only takes two steps to have it in operation.



## First steps

First of all, we need to follow the instructions of WebSploit Labs in order to install the lab.
The full instructions can be found [here](https://websploit.org).



### STEP 1: Download Kali or Parrot

Download Kali or Parrot OS (your preference) and install any of those distributions in a VM. Use the hypervisor of your choice (e.g., VirtualBox, VMWare Workstation/Fusion, ESXi, KVM, Proxmox, etc.).

Minimum VM Requirements:
- 4GB RAM
- 2 vCPU
- 50 GB HDD

#### STEP 1.1: Install multiple utilities

Metapackages are used to install many packages at one time, created as a list of dependencies on other packages. Kali Linux uses these in a few ways. One way is allowing users to decide how many packages out of the total Kali list they would like to install. Need just enough to use Linux? Want enough to conduct Pentests? Perhaps nearly every package available in Kali?

To install a metapackage, we are going to first update the system. While not mandatory, this step is highly recommended to make sure the metapackages can be installed without any unexpected side-effects. The procedure to update Kali is documented in details on the page Updating Kali, but in short, it boils down to two commands:

```shell
sudo apt update
sudo apt full-upgrade -y
```

The step above might take a while, depending on how many packages need to be updated. After it’s complete, installing a metapackage (kali-linux-default in this example) is simply a matter of running one command:

```shell
sudo apt install -y kali-linux-default
```

Alternatively we can use kali-tweaks to install metapackage groups for us. We first run the following command:

```shell
kali-tweaks
```

From here we will navigate into the **“Metapackages”** tab. Now we just select which metapackages we want and then we cant hit **“Apply”** then **“OK”** and finally supply our password.

System:
- `kali-linux-core`: Base Kali Linux System – core items that are always included
- `kali-linux-headless`: Default install that doesn’t require GUI
- `kali-linux-default`: “Default” desktop images include these tools
- `kali-linux-arm`: All tools suitable for ARM devices
- `kali-linux-nethunter`: Tools used as part of Kali NetHunter

Desktop environments/Window managers:
- `kali-desktop-core`: Any key tools required for a GUI image
- `kali-desktop-e17`: Enlightenment (WM)
- `kali-desktop-gnome`: GNOME (DE)
- `kali-desktop-i3`: i3 (WM)
- `kali-desktop-kde`: KDE (DE)
- `kali-desktop-lxde`: LXDE (WM)
- `kali-desktop-mate`: MATE (DE)
- `kali-desktop-xfce`: Xfce (WM)

Tools:
- `kali-tools-gpu`: Tools which benefit from having access to GPU hardware
- `kali-tools-hardware`: Hardware hacking tools
- `kali-tools-crypto-stego`: Tools based around Cryptography & Steganography
- `kali-tools-fuzzing`: For fuzzing protocols
- `kali-tools-802-11`: 802.11 (Commonly known as “Wi-Fi”)
- `kali-tools-bluetooth`: For targeting Bluetooth devices
- `kali-tools-rfid`: Radio-Frequency IDentification tools
- `kali-tools-sdr`: Software-Defined Radio tools
- `kali-tools-voip`: Voice over IP tools
- `kali-tools-windows-resources`: Any resources which can be executed on a Windows hosts
- `kali-linux-labs`: Environments for learning and practising on

Menu:
- `kali-tools-information-gathering`: Used for Open Source Intelligence (OSINT) & information gathering
- `kali-tools-vulnerability`: Vulnerability assessments tools
- `kali-tools-web`: Designed doing web applications attacks
- `kali-tools-database`: Based around any database attacks
- `kali-tools-passwords`: Helpful for password cracking attacks – Online & offline
- `kali-tools-wireless`: All tools based around Wireless protocols – 802.11, Bluetooth, RFID & SDR
- `kali-tools-reverse-engineering`: For reverse engineering binaries
- `kali-tools-exploitation`: Commonly used for doing exploitation
- `kali-tools-social-engineering`: Aimed for doing social engineering techniques
- `kali-tools-sniffing-spoofing`: Any tools meant for sniffing & spoofing
- `kali-tools-post-exploitation`: Techniques for post exploitation stage
- `kali-tools-forensics`: Forensic tools – Live & Offline
- `kali-tools-reporting`: Reporting tools

Others:
- `kali-linux-large`: Our previous default tools for images
- `kali-linux-everything`: Every metapackage and tool listed here
- `kali-desktop-live`: Used during a live session when booted from the image



#### STEP 1.2: Install Flatpak

On Kali Linux, flatpak can be installed through:

```shell
sudo apt install -y flatpak
sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
```

It is also a good idea to install the Flatpak plugin for GNOME Software. To do this, run:

```shell
sudo apt install gnome-software-plugin-flatpak
```

Now you can install new flatpak apps from the software center.

If you want to make flatpak apps look more consistent with the system you can force them to use your local themes:

```shell
mkdir -p ~/.themes
cp -a /usr/share/themes/* ~/.themes/
sudo flatpak override --filesystem=~/.themes/
```



### STEP 2: 

### STEP 2.1: Run the WebSploit Install Script - OLD

After you have installed Kali Linux, run the following command from a terminal window to setup your environment:

```shell
curl -sSL https://websploit.org/install.sh | sudo bash
```

This command will install all the tools, Docker, the intentionally vulnerable containers, and numerous cybersecurity resources.

> **_NOTE:_**  
> WebSploit Labs is not supported in Apple M1/M2 Macs due to compatibility issues with hypervisors and Docker.  
> You can verify the `install.sh` script **SHA-512 checksum** [here](https://websploit.org/sha.html).



### STEP 2.2: Install via Docker-Compose - NEW

First, install Docker if you haven't already. You can follow the official documentation [here](https://docs.docker.com/engine/install).  
Do not forget to run Docker without being root. [Here](https://docs.docker.com/engine/install/linux-postinstall/) are instructions on how to do this.

Now install Docker Compose in your system.

```shell
sudo apt update
sudo apt install -y docker-compose
```

or

```shell
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

Next, download the `docker-compose.yml` file and start the services.

```shell
wget https://websploit.org/docker-compose.yml
docker-compose -f docker-compose.yml up -d
```

> **_NOTE:_**  
> For newer versions of Docker, use `docker compose` instead of `docker-compose`.

All the services are now running.

If you do not want to start all the containers at the same time, you can build one or multiple services specifying the service you want to build:

```shell
docker-compose -f docker-compose.yml up -d [service]
```

Then, to stop and start the services:

```shell
docker-compose -f docker-compose.yml stop [service]
docker-compose -f docker-compose.yml start [service]
```

Finally, if you want to delete one or all the services:

```shell
docker-compose -f docker-compose.yml down [service]
```

Here you can se the list of all the machines available:

- webgoat (websploit)
- juice-shop (websploit)
- dvwa (websploit)
- mutillidae_2 (websploit)
- dvna (websploit)
- hackazon (websploit)
- hackme-rtov (websploit)
- mayhem (websploit)
- rtv-safemode (websploit)
- galactic-archives (websploit)
- yascon-hackme (websploit)
- secretcorp-branch1 (websploit)
- gravemind (websploit)
- dc30_01 (websploit)
- dc30_02 (websploit)
- Y-wing (websploit)
- dc31_01 (websploit2)
- dc31_02 (websploit2)
- dc31_03 (websploit2)
- nodegoat (websploitnet2) - Extra

Now you can install all the tools that Omar Santos encourage to use.

List of tools:
- Git repositories:
  - [H4cker](https://github.com/The-Art-of-Hacking/h4cker.git): This repository contains various resources and tools for hacking, penetration testing, cybersecurity, and information security.
  - [SecLists](https://github.com/danielmiessler/SecLists.git): A collection of multiple types of lists used during security assessments, like usernames, passwords, URLs, sensitive data patterns, fuzzing payloads, web shells, and more.
  - [GitTools](https://github.com/internetwache/GitTools.git): A set of tools for working with Git repositories, including finding sensitive data in repositories and automating interactions with them.
  - [PayloadsAllTheThings](https://github.com/swisskyrepo/PayloadsAllTheThings.git): A list of useful payloads and bypasses for Web Application Security and Pentest/CTF.
  - [IoTGoat](https://github.com/OWASP/IoTGoat/releases/download/v1.0/IoTGoat-raspberry-pi2.img): An intentionally insecure firmware based on OpenWrt. The project is designed to educate on the security issues in IoT and embedded devices.
  - [IoTGoat](https://github.com/santosomar/DVRF/releases/download/v3/DVRF_v03.bin) (Custom Omar Santos firmware)
  - [Radamsa](https://gitlab.com/akihe/radamsa.git): A test case generator that produces random data for testing the robustness of software, useful for fuzz testing.
  - [Sublist3r](https://github.com/aboul3la/Sublist3r.git): A tool designed to enumerate subdomains of websites using OSINT.
  - [Enum4linux-ng](https://github.com/cddmp/enum4linux-ng): A tool for enumerating information from Windows and Samba systems.
  - [Searchsploit](https://github.com/offensive-security/exploitdb.git): An offline copy of the Exploit Database, a collection of exploits, shellcodes, and security papers.
  - [NodeGoat](https://github.com/OWASP/NodeGoat.git): A deliberately vulnerable version of a Node.js application to learn and practice web security.
  - [NodeGoat](https://websploit.org/nodegoat-docker-compose.yml) (Custom Omar Santos docker-compose)
  - [NodeGoat](https://websploit.org/nodegoat.sh) (Custom Omar Santos script)
  - [Knock](https://github.com/guelfoweb/knock.git): A python tool designed to enumerate subdomains on a target domain through a wordlist.
- Apps:
  - Hostapd: A user space daemon for access point and authentication servers.
  - Ffuf: A fast web fuzzer written in Go.
  - Tor: Software for enabling anonymous communication.
  - Certspy: A python tool for inspecting certificate files.
  - Jupyter-notebook: An open-source web application that allows you to create and share documents that contain live code, equations, visualizations, and narrative text.
  - Edb-debugger: A graphical debugger for Linux based on the plugin system.
  - Gobuster: A tool used to brute-force URIs (directories and files) in web sites and DNS subdomains (with wildcard support).
  - Zaproxy: Also known as OWASP ZAP, an open-source web application security scanner.
- Other apps: 
  - Wget: A command-line utility for downloading files from the web.
  - Vim: A highly configurable text editor.
  - Vim-python-jedi: Vim bindings for the Jedi auto-completion library for Python.
  - Curl: A tool for transferring data with URLs.
  - Exuberant-ctags: A multilanguage implementation of ctags.
  - Git: A distributed version-control system.
  - Ack-grep: A tool like grep, optimized for programmers.
  - Python3-pip: The Python package installer.
- Python tools:
  - Gorilla-cli: A CLI for the Gorilla web application security scanner.
- Other Python tools: 
  - Pep8: A tool to check Python code against some of the style conventions in PEP 8.
  - Flake8: A wrapper around PyFlakes, pep8, and Ned Batchelder’s McCabe script.
  - Pyflakes: A simple program that checks Python source files for errors.
  - Isort: A Python utility/library to sort imports alphabetically and automatically separated into sections.
  - Yapf: Yet another Python formatter from Google.)
  - Flask: A micro web framework for Python.



### STEP 3: Docker Service

Docker is NOT configured to start at boot time.
This is to avoid for the vulnerable applications to be exposed by default. To start the Docker service and automatically start the containers use:

```shell
sudo service docker start
sudo service docker status
sudo service docker stop
```

To obtain the status of each docker container use the following command:

```shell
docker ps --format "table {{.Names}}\t{{.Ports}}\t{{.Status}}"
```

Vulnerable Applications deployed with WebSploit Labs:

![Vulnerable Applications](images/readme_01.png)



### STEP 4: Upgrading WebSploit

To upgrade the WebSploit Labs vulnerable containers download and run the `upgrade.sh` script from the command line using the following commands:

```shell
wget https://websploit.org/update.sh
sudo bash update.sh
```



### STEP 5: Use our new testing laboratory

To view the containers, you just have to type their respective IP address and port in your browser.

List of containers with their respective ip addresses and ports:
|     Container          | IP Address | Port               |
|------------------------|------------|--------------------|
| webgoat                |  10.6.6.11 | 8080/tcp, 9090/tcp |
| juice-shop             |  10.6.6.12 |           3000/tcp |
| dvwa                   |  10.6.6.13 |             80/tcp |
| mutillidae_2           |  10.6.6.14 |   80/tcp, 3306/tcp |
| dvna                   |  10.6.6.15 |                    |
| hackazon               |  10.6.6.16 |             80/tcp |
| hackme-rtov            |  10.6.6.17 |             80/tcp |
| mayhem                 |  10.6.6.18 |     22/tcp, 80/tcp |
| rtv-safemode           |  10.6.6.19 |   80/tcp, 3306/tcp |
| galactic-archives      |  10.6.6.20 |           5000/tcp |
| yascon-hackme          |  10.6.6.21 |             80/tcp |
| secretcorp-branch1     |  10.6.6.22 |             80/tcp |
| gravemind              |  10.6.6.23 |                    |
| dc30_01                |  10.6.6.24 |   22/tcp, 3000/tcp |
| dc30_02                |  10.6.6.25 |                    |
| Y-wing                 |  10.6.6.26 |           3000/tcp |
|                        |            |                    |
| dc31_01                |  10.7.7.21 |                    |
| dc31_02                |  10.7.7.22 |           8888/tcp |
| dc31_03                |  10.7.7.23 |           9090/tcp |
|                        |            |                    |
| nodegoat               |            |                    |

In case you have doubts about what type of techniques you will need to use to violate each of them.

I invite you to search and visit the respective official sites of each project.

> **_NOTE:_**  
> All the custom files made by Omar Santos are located in the `resources/websploit-labs/installation` folder.



## Sources

- https://websploit.org
- https://hackblando.com/hacking-home-lab/
- https://www.kali.org/docs/general-use/metapackages/
- https://www.kali.org/docs/tools/flatpak/
- https://docs.docker.com/engine/install/debian/
- https://docs.docker.com/engine/install/linux-postinstall/
- https://docs.docker.com/compose/install/standalone/